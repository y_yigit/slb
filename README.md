### Content repository ###

Deze repository bevat mijn portfolio en SLB opdrachten van jaar 3. Het bestand VOA bevat mijn VOA punten behaald in jaar 2.


### SLB jaar 1 ###
De SLB opdrachten van jaar 1 zijn te vinden op mijn bioinf website via de link: "https://bioinf.nl/~yyigit/slb1.html"

### Themaopdrachten ###

Dit zijn mijn vastgelegde themaopdrachten van jaar 2 en 3.

** Thema 6 **

- In thema 6 heb ik samen met Manon Barelds een aantal stappen in de Galaxy workflow manager doorgelopen. Hierbij hebben we gezocht naar genen betrokken bij Cardiomyopathie. De sequencing data gegenereerd door galaxy werd opgeslagen in MySQL (mariadb.bin) door een python script. Het project is te vinden onder de link: "https://bitbucket.org/y_yigit/diagnosticgenomeanalysis/src/master/templates/"
 
** Thema 7 **

- In thema 7 heb ik de adaptatie en reactie van de Escherichia coli blootgesteld aan zware atmosfeer vervuiling vastgelegd. Dit werd gedaan door Escherichia coli bacteriën in een vervuild milieu te vergelijken met een controle groep. Een belangrijk resultaat die uit het onderzoek onderzoek is gekomen, is dat de meest vervuilde groep het slechts groeit. Deze conclusie is gebaseerd op data analyse dat is uitgevoerd in R. Het project is te vinden onder de link: "https://bitbucket.org/y_yigit/escherichia-coli-adaptation-and-response-to-exposure-to-heavy/src/master/"

** Thema 8 **

- In Thema 8 heb ik naar de corona trend in India gekeken. Ik heb een SIR model gemaakt in R en verbeterd op basis van meer accurate parameters. Het originele SIR model is gebaseerd op het artikel "Prediction of COVID-19 Disease Progression in India: Under the Effect of National Lockdown" door Chennai Mathematical Institute, Indi. De nieuwe parameters zijn gebaseerd op informatie van meerdere artikelen. Deze artikelen staan vermeld in het project "https://bitbucket.org/y_yigit/practicum-08/src/master/"

** Thema 9 **

- In thema 9 heb ik antilichamen onderzocht die aanwezig zijn in de aandoening Systemic sclerosis (SSc). Het doel hiervan was om de twee subcategorieën diffuse systemic sclerosis (DSSc) en limited systemic sclerosis (LSSc) te onderscheiden, dit is belangrijk voor de behandeling van een patiënt. Het project is uitgevoerd in in Weka en R. Dit practicum was een introductie voor het gebruik van Weka, daarom heb ik hier de simpele machine learning classifier OneR gebruikt. Het project is te vinden onder de link: "https://bitbucket.org/y_yigit/thema09/src/master/"

** Thema 10 **

- In Thema 10 heb ik samen met Nils Mooldijk een moleculaire presentatie tool gemaakt. Het maakt
gebruik van de API's Web3DMol, QRCode.js en de RCSB PDB Data API. QRCode.js genereert QRCODES van een RCSB molecuul. Na het scannen van de code presenteert de website door middel van Web3DMol de molecuulformule. Het project is te vinden onder de link: "https://bitbucket.org/y_yigit/thema10/src/master/"

** Thema 11/12 **

- Deze thema's zijn gekoppeld en komen uit mijn minor. Ik heb in dit project de gastro-intestinale microbiota en de voedselinname van in relatie van de allergie sterkte in kinderen met een pinda-allergie vastgesteld. Het onderzoek bevatte een exploratory data analysis uitgevoerd in R en Weka. Het effect van de voedselinname en de microbiota op de sterkte van de allergie werd onderzocht in een lineaire regressie model. Het project is te vinden onder de link: "https://bitbucket.org/y_yigit/digestiv-study/src/master/".
